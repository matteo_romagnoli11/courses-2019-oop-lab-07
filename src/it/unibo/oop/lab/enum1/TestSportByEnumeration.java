package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;
/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    	final SportSocialNetworkUserImpl<User> vrossi = new SportSocialNetworkUserImpl<User>("Valentino", "Rossi", "vrossi", 40);
        final SportSocialNetworkUserImpl<User> becclestone = new SportSocialNetworkUserImpl<User>("Bernie", "Ecclestone", "becclestone", 83);
        final SportSocialNetworkUserImpl<User> falonso = new SportSocialNetworkUserImpl<User>("Fernando", "Alonso", "falonso", 34);
        
        falonso.addSport(Sport.F1);
        falonso.addSport(Sport.SOCCER);
        falonso.addSport(Sport.BIKE);
        System.out.println("Alonso practices F1: " + falonso.hasSport(Sport.F1));
        System.out.println("Alonso does not like volley: " + !falonso.hasSport(Sport.VOLLEY));

        vrossi.addSport(Sport.F1);
        vrossi.addSport(Sport.MOTOGP);

        System.out.println("Rossi has been a professional pilot in MOTOGP: "
                + vrossi.hasSport(Sport.MOTOGP));
        System.out.println("Rossi does not like soccer: " + !vrossi.hasSport(Sport.SOCCER));

        becclestone.addSport(Sport.F1);
        becclestone.addSport(Sport.BASKET);

        System.out.println("Bernie's the boss when it comes to F1: "
                + becclestone.hasSport(Sport.F1));
        System.out.println("Bernie does love playing also basket: "
                + becclestone.hasSport(Sport.BASKET));
    }

}
